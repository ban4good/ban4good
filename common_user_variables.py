# This is a common file with all the variables already defined



json_file_to_read = 'config.json'


# all special strings
shell_regex = r"(([A-z]{3} [0-9]{1,2}) ([0-9]{2}:[0-9]{2}:[0-9]{2}))(?:.*)(?:[F|f]ailed)(?:.*)(?:(?:\b((?:\d{1,3}\.){3}\d{1,3})\b).(?:port (?:\b(\d+)\b)))"
shell_datetime = "%b %d %H:%M:%S"

shell_dict = {}
shell_dict['full_regex'] = ''
shell_dict['time_format'] = ''

joomla_dict = {}
joomla_dict['redirection_path'] = ''

wordpress_dict = {}
wordpress_dict['redirection_path'] = ''

phpmyadmin_dict = {}
phpmyadmin_dict['redirection_path'] = ''

apache_dict = {}
apache_dict['part_1_regex'] = ''
apache_dict['part_2_regex'] = ''
apache_dict['joomla'] = []
apache_dict['wordpress'] = []
apache_dict['phpmyadmin'] = []
