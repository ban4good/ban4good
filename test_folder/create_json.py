#!/usr/bin/env python
import json

json_file_to_write = '../config.json'

# all special strings
shell_regex = r"(([A-z]{3}[ ]*[0-9]{1,2})[ ]*([0-9]{2}:[0-9]{2}:[0-9]{2}))(?:.*)(?:[F|f]ailed)(?:.*)(?:(?:\b((?:\d{1,3}\.){3}\d{1,3})\b).(?:port (?:\b(\d+)\b)))"
shell_datetime = "%b %d %Y %H:%M:%S"

auth_log_file = '/var/log/auth.log'
apache_log_file = '/var/log/apache2/access.log'

shell_dict = {}
shell_dict['full_regex'] = shell_regex
shell_dict['time_format'] = shell_datetime

joomla_dict = {}
joomla_dict['redirection_path'] = 'log-out'

wordpress_dict = {}
wordpress_dict['redirection_path'] = 'wp-login.php'

phpmyadmin_dict = {}
phpmyadmin_dict['redirection_path'] = '/login.php'

apache_part_1_regex = r'(?:\b((?:\d{1,3}\.){3}\d{1,3})\b)(?:.*)(?:\[(([\d]{1,2}/[\w]{3}/[\d]{4}):([\d]{1,2}:[\d]{1,2}:[\d]{1,2}))) \+[\d]{4}\](?:.*)(?:(?:GET)|(?:POST))(?:.*)(?:'
apache_part_2_regex = ')(?:.*)(?:HTTPS?)(?:.*)(?:200)'
apache_dict = {}
apache_dict['part_1_regex'] = apache_part_1_regex
apache_dict['part_2_regex'] = apache_part_2_regex
apache_dict['time_format'] = '%d/%b/%Y:%H:%M:%S'
apache_dict['joomla'] = []
apache_dict['wordpress'] = []
apache_dict['phpmyadmin'] = []

apache_dict['joomla'].append(joomla_dict)
apache_dict['wordpress'].append(wordpress_dict)
apache_dict['phpmyadmin'].append(phpmyadmin_dict)

user_max_tries = 3
user_max_time = 1 # in min
user_block_length = 1 # in min

data = {}

data['shell'] = []
data['apache'] = []
data['shell'].append(shell_dict)
data['apache'].append(apache_dict)
data['user_max_tries'] = user_max_tries
data['user_max_times'] = user_max_time
data['user_block_length'] = user_block_length
data['auth_log_file'] = auth_log_file
data['apache_log_file'] = apache_log_file
print data

with open(json_file_to_write, 'w') as outfile:
    json.dump(data, outfile, sort_keys=True, indent=4, ensure_ascii=False)