import json
from sys import stdout
json_file_to_read = 'config.json'


# all special strings
shell_regex = r"(([A-z]{3} [0-9]{1,2}) ([0-9]{2}:[0-9]{2}:[0-9]{2}))(?:.*)(?:[F|f]ailed)(?:.*)(?:(?:\b((?:\d{1,3}\.){3}\d{1,3})\b).(?:port (?:\b(\d+)\b)))"
shell_datetime = "%b %d %H:%M:%S"

shell_dict = {}
shell_dict['full_regex'] = ''
shell_dict['time_format'] = ''

joomla_dict = {}
joomla_dict['redirection_path'] = ''

wordpress_dict = {}
wordpress_dict['redirection_path'] = ''

phpmyadmin_dict = {}
phpmyadmin_dict['redirection_path'] = ''

apache_dict = {}
apache_dict['part_1_regex'] = ''
apache_dict['part_2_regex'] = ''
apache_dict['joomla'] = []
apache_dict['wordpress'] = []
apache_dict['phpmyadmin'] = []


with open(json_file_to_read, 'r') as json_file:
    data = json.load(json_file)
    # get shell directory
    shell_dict['full_regex'] = data['shell'][0]['full_regex']
    shell_dict['time_format'] = data['shell'][0]['time_format']
    # get apache directory
    apache_dict['part_1_regex'] = data['apache'][0]['part_1_regex']
    apache_dict['part_2_regex'] = data['apache'][0]['part_2_regex']
    # get joomla
    joomla_dict['redirection_path'] = data['apache'][0]['joomla'][0]['redirection_path']
    # get wordpress
    wordpress_dict['redirection_path'] = data['apache'][0]['wordpress'][0]['redirection_path']
    # get phpmyadmin
    phpmyadmin_dict['redirection_path'] = data['apache'][0]['phpmyadmin'][0]['redirection_path']

apache_dict['joomla'].append(joomla_dict)
apache_dict['wordpress'].append(wordpress_dict)
apache_dict['phpmyadmin'].append(phpmyadmin_dict)

print apache_dict

json.dump(data, stdout, sort_keys=True, indent=4, ensure_ascii=False)