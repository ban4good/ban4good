#!/usr/bin/env bash

sudo cd /etc/ban4good/test_folder

echo "Creating config.json"

./create_json.py

cd /etc/ban4good

if [ -e /etc/ban4good/ban4good.db ]
then
	echo "Removing database"
    sudo rm /etc/ban4good/ban4good.db
else
    echo "No initial database"
fi

echo "Setting up sqlite3 db"

/etc/ban4good/db_conn.py

echo "Time to create new user"
/etc/ban4good/login.py

echo "adding the run.sh to ~/.bashrc"

sudo echo "/etc/ban4good/run.sh &" >> ~/.bashrc

echo "restarting ~/.bashrc"
source ~/.bashrc

/etc/ban4good/run.sh &

echo "Now follow the procedure 4 and 5 in readme.txt file"

sleep 2

vi /etc/ban4good/readme.txt