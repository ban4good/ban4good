import json
from sys import stdout
from os import path



MY_DIRECTORY = path.dirname(path.realpath(__file__))
json_config_file = MY_DIRECTORY + "/config.json"

curr_data = {}

max_request = 0
max_time = 0
timeout_length = 0

shell_dict = {}
shell_dict['full_regex'] = ''
shell_dict['time_format'] = ''

joomla_dict = {}
joomla_dict['redirection_path'] = ''

wordpress_dict = {}
wordpress_dict['redirection_path'] = ''

phpmyadmin_dict = {}
phpmyadmin_dict['redirection_path'] = ''

apache_dict = {}
apache_dict['part_1_regex'] = ''
apache_dict['part_2_regex'] = ''
apache_dict['time_format'] = ''
apache_dict['joomla'] = []
apache_dict['wordpress'] = []
apache_dict['phpmyadmin'] = []

auth_log_file = ''
apache_log_file = ''

def read_file():
    global json_config_file
    global shell_dict
    global joomla_dict
    global wordpress_dict
    global phpmyadmin_dict
    global apache_dict
    global max_request
    global max_time
    global timeout_length
    global auth_log_file
    global apache_log_file
    global curr_data
    with open(json_config_file, 'r') as json_file:
        data = json.load(json_file)
        # get shell directory
        shell_dict['full_regex'] = data['shell'][0]['full_regex']
        shell_dict['time_format'] = data['shell'][0]['time_format']
        # get apache directory
        apache_dict['part_1_regex'] = data['apache'][0]['part_1_regex']
        apache_dict['part_2_regex'] = data['apache'][0]['part_2_regex']
        apache_dict['time_format'] = data['apache'][0]['time_format']
        # get joomla
        joomla_dict['redirection_path'] = data['apache'][0]['joomla'][0]['redirection_path']
        # get wordpress
        wordpress_dict['redirection_path'] = data['apache'][0]['wordpress'][0]['redirection_path']
        # get phpmyadmin
        phpmyadmin_dict['redirection_path'] = data['apache'][0]['phpmyadmin'][0]['redirection_path']

        max_request = data['user_max_tries']
        max_time = data['user_max_times']
        timeout_length = data['user_block_length']
        auth_log_file = data['auth_log_file']
        apache_log_file = data['apache_log_file']
        curr_data = data
    return curr_data

def write_file(data):
    with open(json_config_file, 'w') as outfile:
        json.dump(data, outfile, sort_keys=True, indent=4, ensure_ascii=False)

def update_joomla_redirection(redirection_path):
    global joomla_dict
    joomla_dict['redirection_path'] = redirection_path;
def update_wordpress_redirection(redirection_path):
    global wordpress_dict
    wordpress_dict['redirection_path'] = redirection_path;
def update_phpmyadmin_redirection(redirection_path):
    global phpmyadmin_dict
    phpmyadmin_dict['redirection_path'] = redirection_path;
def update_auth_log_file(new_path):
    global auth_log_file
    auth_log_file = new_path
def update_apache_log_file(new_path):
    global apache_log_file
    apache_log_file = new_path

def print_all_variables():
    global json_config_file
    global shell_dict
    global joomla_dict
    global wordpress_dict
    global phpmyadmin_dict
    global apache_dict
    global max_request
    global max_time
    global timeout_length
    data = {}
    data['shell'] = []
    data['apache'] = []
    data['shell'].append(shell_dict)
    data['apache'].append(apache_dict)
    data['user_max_tries'] = max_request
    data['user_max_times'] = max_time
    data['user_block_length'] = timeout_length
    json.dump(data, stdout, sort_keys=True, indent=4, ensure_ascii=False)

