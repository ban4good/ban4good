#!/usr/bin/env bash

while true
do
printf "\n" &>> /etc/ban4good/log/ban4good_reading.log
date '+%F %H:%M:%S' &>> /etc/ban4good/log/ban4good_reading.log
/etc/ban4good/read_files.py &>> /etc/ban4good/log/ban4good_reading.log

printf "\n" &>> /etc/ban4good/log/ban4good_reading.log
date '+%F %H:%M:%S' &>> /etc/ban4good/log/ban4good_reading.log
/etc/ban4good/check_db_for_blocking.py &>> /etc/ban4good/log/ban4good_blocking.log
sleep 2
done
