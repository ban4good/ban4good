#!/usr/bin/env python

import hashlib
from binascii import hexlify
from random import sample
import string
import sqlite3
from sqlite3 import Error
from getpass import getpass
from sys import version_info


def connect_to_db():
    return sqlite3.connect('/etc/ban4good/ban4good.db')

def user_hash(username):
    username = username.encode('utf-8')
    return hashlib.pbkdf2_hmac('sha256', username, b'' , 1)

def pass_hash(password, salt):
    password = password.encode('utf-8')
    salt = salt.encode('utf-8')
    return hashlib.pbkdf2_hmac('sha256', password, salt , 100)


def create_new_user(username, password):
    conn = connect_to_db()
    conn.text_factory = lambda x: unicode(x, "utf-8", "ignore")
    c = conn.cursor()

    salt = ''.join(sample((string.ascii_uppercase+string.ascii_lowercase+string.digits),40))
    username = user_hash(username)
    password = pass_hash(password, salt)

    sql = 'INSERT INTO secure_login(salt, username, password) VALUES(?, ?, ?)';
    try:
        with conn:
            c.execute(sql, (salt, username, password))
    except Error as e:
        print(e)
        print("User exists")
        return False
    conn.close()
    return True

def check_user(username, password):
    conn = connect_to_db()
    c = conn.cursor()

    username = user_hash(username)

    sql = 'SELECT * FROM secure_login WHERE username = ?'
    c.execute(sql,(username))
    row = c.fetchone()
    if row:
        salt = row['salt']
        db_password = row['password']
        if db_password == pass_hash(password, salt):
            return True
    return False

if __name__ == '__main__':
    py3 = version_info[0] > 2
    username = None
    password = None
    allow_user = False
    print('\033[93m\033[1m' + "Please make the username and password so that you can login later in the website.\nRemember you cannot renew your password. You will have to run this setup page again if you want to renew the password.\n\n" + '\033[0m')
    while not allow_user:
        if py3:
            username = input("Enter new username: ").strip()
        else:
            username = raw_input("Enter new username: ").strip()
        if username and username is not '':
            allow_user = True
            allow_pass = False
            while not allow_pass:
                password = getpass("Enter new Password: ")
                confirm_password = getpass("confirm password: ")
                if password != confirm_password:
                    print("Passowrd does not match\n")
                else:
                    allow_pass = True
    create_new_user(username, password)

    exit(2)




