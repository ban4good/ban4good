#!/usr/bin/env python
import db_conn
import handle_config
from time import sleep


if __name__ == '__main__':
    handle_config.read_file()
    db_conn.get_ip_to_block_unblock(handle_config.max_request, handle_config.max_time)
    db_conn.remove_block_ip_status()
