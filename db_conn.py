#!/usr/bin/env python

import sqlite3
from sqlite3 import Error

def connect_to_db():
    return sqlite3.connect('ban4good.db')

def set_up_db():
    conn = connect_to_db()
    c = conn.cursor()
    drop_ip_log = "DROP TABLE IF EXISTS ip_log"

    secure_login_query = "CREATE TABLE IF NOT EXISTS secure_login (\
                salt VARCHAR(255) NOT NULL PRIMARY KEY,\
                username VARCHAR(255) NOT NULL,\
                password VACHAR(255) NOT NULL\
            )"
    ip_log_query = "CREATE TABLE IF NOT EXISTS ip_log (\
                id INT AUTO_INCREMENT PRIMARY KEY,\
                ip VARCHAR(20) NOT NULL,\
                port INT DEFAULT NULL,\
                access_time DATETIME NOT NULL,\
                access_location VARCHAR(30) DEFAULT NULL\
            )"
    blocked_ip = "CREATE TABLE IF NOT EXISTS blocked_ip (\
                ip VARCHAR(20) NOT NULL PRIMARY KEY,\
                blocked_time DATETIME NOT NULL,\
                blocked_until DATETIME NOT NULL\
            )"
    try:
        c.execute(drop_ip_log)
        c.execute(secure_login_query)
        c.execute(ip_log_query)
        c.execute(blocked_ip)
    except Error as e:
        print(e)
        return False
    conn.close()
    return True

def insert_new_user(salt, username, password):
    conn = connect_to_db()
    c = conn.cursor()
    try:
        c.execute("INSERT INTO secure_login(salt, username, password) VALUES(?, ?, ?)", (salt, username, password))
    except Error as e:
        print(e)
        return False
    conn.close()
    return True

def insert_ip_log(ip, port, access_time, access_location):
    conn = connect_to_db()
    try:
        with conn:
            sql = "INSERT INTO ip_log(ip, port, access_time, access_location) VALUES('%s', '%s', '%s', '%s')" %(ip, port, access_time, access_location)
            # print(sql)
            conn.execute(sql)
    except Error as e:
        print(e)
        return False
    conn.close()
    return True

def insert_blocked_ip(ip, blocked_time, blocked_until):
    conn = connect_to_db()
    c = conn.cursor()
    sql = "INSERT INTO blocked_ip(ip, blocked_time, blocked_until) VALUES('%s', '%s', '%s')" %(ip, blocked_time, blocked_until)
    print(sql)
    try:
        with conn:
            c.execute(sql)

    except Error as e:
        print(e)
        return False
    conn.close()
    return True

def remove_block_ip_status():
    from datetime import datetime
    from datetime import timedelta
    import ip_block_unblock
    current_time = datetime.now().isoformat(' ')
    conn = connect_to_db()
    c = conn.cursor()
    sql = "SELECT ip FROM blocked_ip WHERE blocked_until < '%s'" %(current_time)
    print(sql)
    c.execute(sql)
    rows = c.fetchall()
    for row in rows:
        ip_block_unblock.un_block_ip(row[0])
        try:
            sql = "DELETE FROM blocked_ip  WHERE ip = '%s'" %(row[0])
            c.execute(sql)
            conn.commit()
        except Error as e:
            print(e)
            return False
    conn.close()
    return True

def get_ip_to_block_unblock(count, timeout_length):
    conn = connect_to_db()
    c = conn.cursor()
    try:
        from datetime import datetime
        from datetime import timedelta
        import ip_block_unblock
        current_time = datetime.now()
        # block_from = format_time(current_time - timedelta(minutes=int(timeout_length)))
        block_from = (current_time - timedelta(minutes=int(timeout_length))).isoformat(' ')
        sql = "SELECT ip FROM ip_log WHERE access_time > '%s' GROUP BY ip HAVING count(*) >= %s" %(block_from, count)
        print(sql)
        c.execute(sql)
        rows = c.fetchall()
        # block ip
        block_till = (current_time + timedelta(minutes=int(timeout_length))).isoformat(' ')
        current_time = current_time.isoformat(' ')
        for row in rows:
            # print(row)
            
            if insert_blocked_ip(row[0], current_time , block_till) is True:
                ip_block_unblock.block_ip(row[0])
            
    except Error as e:
        print(e)
        return False
    remove_block_ip_status()

    conn.close()
    return True

def format_time(my_time):
    from time import strftime
    return strftime("%Y-%m-%d %H:%M:%S", my_time)

if __name__ == '__main__':
    set_up_db()