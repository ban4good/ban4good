<?php

function sec_session_start() {
		$session_name = 'sec_session_id';
		// This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session 
    session_regenerate_id();    // regenerated the session, delete the old one. 
}

	function user_hash($username){
		$hash = hash_pbkdf2("sha256", $username, '', 1, 64);
		return $hash;
	}

	function pass_hash($password, $salt){
		$iterations = 100;
		$hash = hash_pbkdf2("sha256", $password, $salt, $iterations, 64);
		return $hash;
	}

	function login_user($username , $password){
		$db = new SQLite3('/etc/ban4good/ban4good.db');
		$results = $db->querySingle('SELECT * FROM secure_login WHERE username = $username');
		if ($results != null){
			$salt = row['salt'];
			$db_password = row['password'];
			$pw = pass_hash($password, $salt);
			if($pw == $password){
				$salt = row['salt'];
				$db_password = row['password'];
				$username = row['username'];
				$db_cookie_setup = $db_password + $username + $salt;
				$db_cookie_hash = pass_hash($db_cookie_setup, $salt);
					setcookie('user_cardential', $db_cookie_hash, time() + (86400 * 361), "/"); // 86400 = 1 day
			}
		}
		return False;
	}

	function check_user_login() {
		if(isset($_COOKIE['user_cardential'])) {
			$db = new SQLite3('/etc/ban4good/ban4good.db');
			$results = $db->querySingle('SELECT * FROM secure_login LIMIT 1');
			if($results != null) {
				$salt = row['salt'];
				$db_password = row['password'];
				$username = row['username'];
				$db_cookie_setup = $db_password + $username + $salt;
				$db_cookie_hash = pass_hash($db_cookie_setup, $salt);
				if($db_cookie_hash == $_COOKIE['user_cardential']) {
					return true;
				}
			}
		}
		return false;
	}

function my_encript($text_to_encript) {
    $encryptionMethod = "AES-256-CBC";
    $secretHash = pass_hash($_COOKIE['user_cardential'], $encryptionMethod);
    $encripted = openssl_encrypt($text_to_encript, $encryptionMethod, $secretHash);
    return $encripted;
}

function my_decript($encryptedMessage) {
    $encryptionMethod = "AES-256-CBC";
    $secretHash = pass_hash($_COOKIE['user_cardential'], $encryptionMethod);
    $decrepted = openssl_decrypt($encryptedMessage, $encryptionMethod, $secretHash);
    return $decrepted;
}

function remove_user_from_iptable() {
	// remoe users
}

?>
