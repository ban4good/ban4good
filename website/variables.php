<?php

$header_tag = '

<html lang="en">
<head>
    <title>Ban4Good</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/web.css">


    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- My JS -->
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body>
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
        <a class="navbar-brand" href="#">
            <img class="nav-img-sizing" src="img/logo.png" />
        </a>
        <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#navbar-contents-dropdown" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbar-contents-dropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="#" class="btn">
                        <span class="nav-elements" data-toggle="tooltip" title="LOGOUT">
                            <i class="fa fa-2x" aria-hidden="true" disabled></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
';

$login_page = '

    <div class="container">
        <div class="row spacer-row">
            <!-- This is just a spacer row -->
        </div>

        <div class="row">
            <div class="col-12">
                <div class="row">
                    <form id="login_form" class="form-horizontal w-100" role="form" type="POST">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <h2>Login</h2>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="sr-only" for="login-username">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user-o"></i>
                                        </div>
                                        <input type="text" name="username" class="form-control" id="login-username" placeholder="Username" required autofocus>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="sr-only" for="login-password">Password</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-key"></i>
                                        </div>
                                        <input type="password" name="password" class="form-control" id="login-password" placeholder="Password" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <button id="login_button" type="submit" class="btn btn-primary w-25"><i class="fa fa-sign-in"></i> Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

';

$request_page ='  <div class="container">
  <div class="row spacer-row">
  </div>
  <form  action="" method="post" id="request_form">
    <div class="form-group row">
      <label for="example-text-input" class="col-2 col-form-label">X Value</label>
        <div class="col-10">
          <label class="sr-only" for="inlineFormInput">X</label>
          <input type="number" name="xval"class="form-control col-mb-4 mr-2 " id="inlineFormInput" min="1" placeholder="X Value">
        </div>
    </div>
    <div class="form-group row">
      <label for="example-text-input" class="col-2 col-form-label">Y Value</label>
        <div class="col-10">
          <label class="sr-only" for="inlineFormInput">Y</label>
          <input type="number" name="yval" class="form-control col-mb-4 mr-2 " id="inlineFormInput" min="1" placeholder="Y Value">
        </div>
    </div>
    <div class="form-group row">
      <label for="example-text-input" class="col-2 col-form-label">Z Value</label>
        <div class="col-10">
          <label class="sr-only" for="inlineFormInput">Z</label>
          <input type="number" name="zval" class="form-control col-mb-4 mr-2 " id="inlineFormInput" min="1" placeholder="Z Value">
        </div>
    </div>
      <div class="row spacer-row">
      </div>
      <div class="row">

          <div class="col-md-5">
              <label class="sr-only" for="inlineFormInput">submit</label>
              <button type="submit" class="btn btn-primary form-control mb-3" id="inlineFormInput">Submit</button>
          </div>
          <div class="col-md-5">
              <label class="sr-only" for="inlineFormInput">reset</label>
              <button type="reset" class="btn btn-primary form-control mb-3" id="inlineFormInput">Reset</button>
          </div>
      </div>
  </form>
<div class="row spacer-row">
</div>
';

$table_header ='<div class="col-md-9">
          <table class="table table-bordered table-striped">
              <thead>
                  <tr>
                    <th width="20%">#</th>
                    <th width="40%">IP</th>
                    <th width="40%">Blocked</th>
                  </tr>
              </thead>
              <tbody>
              ';
                
$table_footer = '

</tbody>
        </table>
  </div>

';

$footer_tag = '
</body>
</html>

';

function print_blocked_ip() {
    $db = new SQLite3('/etc/ban4good/ban4good.db');
    $results = $db->query('SELECT * FROM blocked_ip');
    $row = array();
    $i = 1;
    while($res = $results->fetchArray(SQLITE3_ASSOC)) {
        
        echo '
            <tr>
                <td>'.$i.'</td>
                <td>'.$res['ip'].'</td>
                <td>
                    <button type="button" id="'.my_encript($res['ip']).'" class="btn btn-warning delete_ip">Un-block</button>
                </td>
            </tr>

        
        ';
    }
}

function request_page() {
    echo $request_page;
    echo $table_header;
    print_blocked_ip();
    echo $table_footer;
}

?>