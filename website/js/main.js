$( document ).ready(function() {
    $("#inlineFormInput").click(function( event ) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "handle_request.php",
            data: $("#request_form").serialize(),
            success: function(data) {
                location.reload();
            }
        });
    });

    $(".delete_ip").click(function( event ) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "handle_request.php",
            data: { 'delete_name': event.target.id },
            success: function(data) {
                location.reload();
            }
        });
    });
});