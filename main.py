#!/usr/bin/env python
import getopt, sys
import handle_config

def usage():
    print("\
    python ban4good.py [-h] [-x] [# of request] [-y] [in # of min] [-z] [block for min]\
    ")

if __name__ == '__main__':
    curr_data = handle_config.read_file()
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hx:y:z:", ["help", "request=", "minutes=", "blocktime="])
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(2)
        elif o in ("-x", "--request"):
            curr_data['user_max_tries'] = a
        elif o in ("-y", "--minutes"):
            curr_data['user_max_times'] = a
        elif o in ("-z", "--blocktime"):
            curr_data['user_block_length'] = a
        else:
            usage()
            sys.exit(2)
    handle_config.write_file(curr_data)
