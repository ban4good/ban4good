Ban4Good is a simple tool that lets you secure your server from spam login attemps.
We also have a very simple web service to help you control who is blocked.
You can also control the amount of time a user will be blocked, and after how many tries they get to go for.

In order to install the software you have to do the following

1) Install the program by doing
        git clone https://manjur-khan@bitbucket.org/ban4good/ban4good.git

2) Move the file named ban4good to your /etc folder. This is very important because everything else depends on the file being in that location.

3) Now run the following command and follow the instructions.
        /etc/ban4good/install.sh
    

4) Move the website folder to where you want to access your website from. Make sure you can log into the website without any problem.

5) There might be some issures in how I am reading the file. So if you want to change the directory fro where the files are being read from you can do so by going to /etc/ban4good/config.json and renaming the variables.
    You can also rename the variables from your website. However file paths can only be renamed by actually going to config.json file.

    Few things explained in config.json
        "redirection_path": is the path that the user go to when they unsuccessfully log in. You do not need to give a very long redirection path, but just long enough that it can be distinguished from other ones.

        "apache_log_file": is where your apache log file is located. Default position for this file is "/var/log/apache2/access.log". We support apache2 only.
        "auth_log_file": is where your bash log file is located. Default position is /var/log/auth.log
        "user_block_length": is how many minutes the user is locked out for from the service. When the user is locked out they cannot log into the file till they are unlocked again. Default length is 1 minutes
        "user_max_times" : this is the how much time the user has in which they can login at most x amount of time unsuccessfully. Default is 5
        "user_max_tries" : is how many times the user gets to try unsuccessful login attemps within an x amount of time before they are locked out of the site. Default is 5

6) A reminder that if you ever lose your login credential you will have to do part 3 and 5 again.