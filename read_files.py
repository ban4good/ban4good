#!/usr/bin/env python

import handle_config
import re
from time import strptime, strftime
from datetime import datetime
import db_conn
import sys
from os import path

apache_file_location = "/var/log/apache2/access.log"
shell_file_location = "/var/log/auth.log"

regex_joomla = ''
regex_wordpress = ''
regex_phpmyadmin = ''

regex_bash = ''

bash_time = ''
apache_time = ''

ip = ''
port = ''
access_time = ''
access_location = ''

MY_DIRECTORY = path.dirname(path.realpath(__file__))
LOG_FILE = MY_DIRECTORY + '/ban4good.log'

def match_regex_shell(line):
    search_obj = re.match(regex_bash, line)
    if(search_obj):
        my_time = strptime(search_obj.group(2) + " " + str(datetime.now().year) + " " + search_obj.group(3), bash_time)
        my_ip = search_obj.group(4)
        my_port = search_obj.group(5)
        return my_ip + "|" + my_port + "|" + format_time(my_time) + "|" + "Shell"

def match_regex_apache(line):
    search_obj = re.match(regex_joomla, line)
    if(search_obj):
        my_time = strptime(search_obj.group(2), apache_time)
        my_ip = search_obj.group(1)
        return my_ip + "|" + format_time(my_time) + "|" + "joomla"

    search_obj = re.match(regex_wordpress, line)
    if(search_obj):
        my_time = strptime(search_obj.group(2), apache_time)
        my_ip = search_obj.group(1)
        return my_ip + "|" + format_time(my_time) + "|" + "wordpress"

    search_obj = re.match(regex_phpmyadmin, line)
    if(search_obj):
        my_time = strptime(search_obj.group(2), apache_time)
        my_ip = search_obj.group(1)
        return my_ip + "|" + format_time(my_time) + "|" + "phpmyadmin"

def format_time(my_time):
    return strftime("%Y-%m-%d %H:%M:%S", my_time)


if __name__ == '__main__':
    handle_config.read_file()
    db_conn.set_up_db()
    regex_bash = handle_config.shell_dict['full_regex']
    regex_joomla = handle_config.apache_dict['part_1_regex'] + handle_config.joomla_dict['redirection_path'] + handle_config.apache_dict['part_2_regex']
    regex_wordpress = handle_config.apache_dict['part_1_regex'] + handle_config.wordpress_dict['redirection_path'] + handle_config.apache_dict['part_2_regex']
    regex_phpmyadmin = handle_config.apache_dict['part_1_regex'] + handle_config.phpmyadmin_dict['redirection_path'] + handle_config.apache_dict['part_2_regex']
    bash_time = handle_config.shell_dict['time_format']
    apache_time = handle_config.apache_dict['time_format']
    apache_file_location = handle_config.apache_log_file
    shell_file_location = handle_config.auth_log_file

    try:
        with open(apache_file_location, 'r') as file:
            for line in file:
                regex_return = match_regex_apache(line)
                if regex_return:
                    # print regex_return
                    ip, access_time, access_location = regex_return.split('|')
                    db_conn.insert_ip_log(ip, None, access_time, access_location)
        with open(shell_file_location, 'r') as file:
            for line in file:
                regex_return = match_regex_shell(line)
                if regex_return:
                    # print regex_return
                    ip, port, access_time, access_location = regex_return.split('|')
                    db_conn.insert_ip_log(ip, port, access_time, access_location)
    except IOError as (errno, strerror):
        # with open(LOG_FILE, 'a') as log_file:
        #     log_file.write("\n" + __file__ + " " + str(datetime.now()) + "\n")
        #     log_file.write("I/O error({0}): {1}".format(errno, strerror))
        print "I/O error({0}): {1}".format(errno, strerror)
    except ValueError as e:
        # with open(LOG_FILE, 'a') as log_file:
        #     log_file.write("\n" + __file__ + " " + str(datetime.now()) + "\n")
        #     log_file.write(e)
        print e
    # except:
        # with open(LOG_FILE, 'a') as log_file:
        #     log_file.write("\n" + __file__ + " " + str(datetime.now()) + "\n")
        #     log_file.write(sys.exc_info()[0])

