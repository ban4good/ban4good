
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class Watcher:
    DIRECTORY_TO_WATCH = "/var/log"

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(2)
        except:
            print "Error happened while observing"
            # self.observer.stop()
            # print "Error"

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            # print "Received created event - %s." % event.src_path
            return None

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            from os import path
            from subprocess import call
            print "Received modified event - %s." % event.src_path
            program_dir = path.dirname(path.realpath(__file__))
            program = program_dir + '/read_files.py'
            shell_cmd = "nohup python " + program + " &"
            call(shell_cmd.split(' '), shell=True)


if __name__ == '__main__':
    w = Watcher()
    w.run()