global json_config_file
global shell_dict
global joomla_dict
global wordpress_dict
global phpmyadmin_dict
global apache_dict
global max_request
global max_time
global timeout_length

def init():
  
    json_config_file = "config.json"

    max_request = 0
    max_time = 0
    timeout_length = 0

    shell_dict = {}
    shell_dict['full_regex'] = ''
    shell_dict['time_format'] = ''

    joomla_dict = {}
    joomla_dict['redirection_path'] = ''

    wordpress_dict = {}
    wordpress_dict['redirection_path'] = ''

    phpmyadmin_dict = {}
    phpmyadmin_dict['redirection_path'] = ''

    apache_dict = {}
    apache_dict['part_1_regex'] = ''
    apache_dict['part_2_regex'] = ''
    apache_dict['time_format'] = ''
    apache_dict['joomla'] = []
    apache_dict['wordpress'] = []
    apache_dict['phpmyadmin'] = []